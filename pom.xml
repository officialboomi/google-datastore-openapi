<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.boomi.connector</groupId>
    <artifactId>openapiconnector-google_datastore</artifactId>
    <version>1.0.2-SNAPSHOT</version>

    <!-- Specification of the Boomi SDK repository -->
    <repositories>
        <repository>
            <id>boomisdk</id>
            <name>Connector SDK Repository</name>
            <url>https://boomisdk.s3.amazonaws.com/releases</url>
        </repository>
    </repositories>

    <properties>
        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>
        <connector.sdk.version>2.14.4</connector.sdk.version>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <maven.release.plugin.version>3.0.0-M6</maven.release.plugin.version>
    </properties>

    <scm>
        <connection>scm:git:file://${env.PWD}/.git</connection>
        <tag>HEAD</tag>
    </scm>

    <dependencies>
        <!-- SDK Dependencies -->
        <dependency>
            <groupId>com.boomi.connsdk</groupId>
            <artifactId>connector-sdk-api</artifactId>
            <version>${connector.sdk.version}</version>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>com.boomi.util</groupId>
            <artifactId>boomi-json-util</artifactId>
            <version>1.2.1</version>
            <scope>compile</scope>
        </dependency>

        <dependency>
            <groupId>com.boomi.util</groupId>
            <artifactId>boomi-util</artifactId>
            <version>2.3.12</version>
            <scope>compile</scope>
        </dependency>

        <dependency>
            <groupId>com.boomi.connsdk</groupId>
            <artifactId>connector-sdk-util</artifactId>
            <version>${connector.sdk.version}</version>
        </dependency>

        <!-- OpenAPI Dependencies -->
        <dependency>
            <groupId>com.boomi.connsdk</groupId>
            <artifactId>connector-sdk-openapi</artifactId>
            <version>${connector.sdk.version}</version>
            <!-- exclusions that are not needed or included by the atom-->
            <exclusions>
                <exclusion>
                    <groupId>com.noelios.restlet</groupId>
                    <artifactId>com.noelios.restlet.ext.net</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>org.restlet</groupId>
                    <artifactId>org.restlet</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>com.google.guava</groupId>
                    <artifactId>guava</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>com.google.code.findbugs</groupId>
                    <artifactId>jsr305</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>javax.mail</groupId>
                    <artifactId>mailapi</artifactId>
                </exclusion>
            </exclusions>
        </dependency>

        <!--Amazon Depedencies -->
        <dependency>
            <groupId>com.boomi.common</groupId>
            <artifactId>common-aws</artifactId>
            <version>1.2.2</version>
        </dependency>

        <!-- Test Dependencies -->
        <dependency>
            <groupId>com.boomi.connsdk</groupId>
            <artifactId>connector-sdk-test-util</artifactId>
            <version>${connector.sdk.version}</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.13.1</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>com.boomi.connsdk</groupId>
            <artifactId>connector-sdk-model</artifactId>
            <version>${connector.sdk.version}</version>
            <scope>test</scope>
        </dependency>

    </dependencies>

    <build>
        <resources>
            <resource>
                <directory>${project.basedir}/src/main/resources/</directory>
                <includes>
                    <include>com.boomi.Overrides</include>
                </includes>
            </resource>
            <resource>
                <directory>${project.basedir}/src/main/resources/google_datastore/</directory>
            </resource>
            <resource>
                <directory>${project.basedir}/src/main/resources/google_datastore/</directory>
                <includes>
                    <include>connector-descriptor-google_datastore.xml</include>
                </includes>
                <targetPath>${project.build.directory}</targetPath>
            </resource>
        </resources>

        <testResources>
            <testResource>
                <directory>${project.basedir}/src/test/resources/</directory>
                <includes>
                    <include>*.*</include>
                </includes>
            </testResource>
            <testResource>
                <directory>${project.basedir}/src/test/resources/google_datastore/</directory>
            </testResource>
        </testResources>

        <testSourceDirectory>${project.basedir}/src/test/java/com/boomi/connector/google_datastore</testSourceDirectory>

        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.8.1</version>
                <configuration>
                    <source>1.8</source>
                    <target>1.8</target>
                    <includes>
                        <include>com/boomi/connector/google_datastore/**/*.java</include>
                    </includes>
                </configuration>
            </plugin>

            <!-- CAR Assembly File Config -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-assembly-plugin</artifactId>
                <configuration combine.self="override">
                    <descriptors>
                        <descriptor>src/assembly/assembly.xml</descriptor>
                    </descriptors>
                </configuration>
                <executions>
                    <execution>
                        <phase>package</phase>
                        <goals>
                            <goal>single</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>3.0.0-M5</version>
                <configuration>
                    <systemPropertyVariables>
                        <javax.xml.accessExternalSchema>all</javax.xml.accessExternalSchema>
                    </systemPropertyVariables>
                </configuration>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-release-plugin</artifactId>
                <version>${maven.release.plugin.version}</version>
                <configuration>
                    <tagNameFormat>@{project.version}</tagNameFormat>
                    <scmReleaseCommitComment>@{prefix} - release tag: '@{releaseLabel}'</scmReleaseCommitComment>
                    <scmDevelopmentCommitComment>[skip ci] @{prefix} - increment version for next development iteration</scmDevelopmentCommitComment>
                </configuration>
            </plugin>
        </plugins>
    </build>
</project>